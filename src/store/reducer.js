const initialState = {
  expression: "",
  result: ""
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "INPUT_CHANGE":
      return {...state, expression: action.value};
    case "BUTTON_INPUT":
      const newExpression = state.expression + action.value;
      return {...state, expression: newExpression};
    case "GET_RESULT":
      let result;
      const mask = /[^\d-+/*() ]/g;
      if (state.expression.match(mask)) {
        result = "INVALID INPUT";
      } else {
        result = eval(state.expression);
      }
      return {...state, result: result};
    case "INPUT_CLEAR":
      return {...state, expression: "", result: ""};
    default:
      return state;
  }
};

export default reducer;