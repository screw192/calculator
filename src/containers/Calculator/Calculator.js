import React from 'react';
import {useDispatch, useSelector} from "react-redux";

import ButtonBlock from "../../components/ButtonBlock/ButtonBlock";
import "./Calculator.css";

const Calculator = () => {
  const dispatch = useDispatch();
  const state = useSelector(state => state);

  const inputChange = event => dispatch({type: "INPUT_CHANGE", value: event.target.value});
  const inputClear = () => dispatch({type: "INPUT_CLEAR"});

  return (
      <div className="CalculatorBlock">
        <input
            type="text"
            placeholder="Enter expression here..."
            autoFocus
            className="ExpressionScreen"
            onChange={inputChange}
            value={state.expression}
        />
        <div type="text" className="ResultScreen">
          {state.result}
        </div>
        <div className="ClearBlock">
          <button className="ClearButton" onClick={inputClear}>
            Clear
          </button>
        </div>
        <ButtonBlock />
      </div>
  );
};

export default Calculator;