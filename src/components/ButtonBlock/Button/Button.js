import React from 'react';

import "./Button.css";

const Button = ({btnClass, label, handler, name}) => {
  return (
      <div className="ButtonCell">
        <button
            className={btnClass.join(" ")}
            type="button"
            name={name}
            onClick={handler}
        >
          {label}
        </button>
      </div>
  );
};

export default Button;