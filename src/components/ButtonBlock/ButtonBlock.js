import React from 'react';
import {useDispatch, useSelector} from "react-redux";

import Button from "./Button/Button";
import "./ButtonBlock.css";

const ButtonBlock = () => {
  const dispatch = useDispatch();
  const state = useSelector(state => state);

  const buttonsArray = [
    '7', '8', '9', '/',
    '4', '5', '6', '*',
    '1', '2', '3', '-',
    '0', '.', '=', '+',
  ];

  const resultButtonHandler = () => dispatch({type: "GET_RESULT"});
  const buttonInputHandler = event => dispatch({type: "BUTTON_INPUT", value: event.target.name});

  const buttons = buttonsArray.map(item => {
    let buttonHandler;
    if (item === "=") {
      buttonHandler = resultButtonHandler;
    } else {
      buttonHandler = buttonInputHandler;
    }

    let buttonClass = ["CalcButton"];
    if ((item >= 0 && item <=9) || item === ".") {
      buttonClass.push("CalcButton_num");
    } else {
      buttonClass.push("CalcButton_operator");
    }

    return (
        <Button
            key={"calcKey_" + item}
            btnClass={buttonClass}
            label={item}
            name={item}
            handler={buttonHandler}
        />
    );
  });

  return (
      <div className="ButtonBlock">
        {buttons}
      </div>
  );
};

export default ButtonBlock;